resource "aws_security_group" "builds_sg" {
  name        = "builds_security group"
  description = "builds security group"
  vpc_id      = "${aws_vpc.builds_vpc.id}"
  tags = { 
	Name = "builds_sg" 
  }
}

resource "aws_security_group_rule" "ingress_all_sg" {
  type        = "ingress"
  from_port   = 0
  to_port     = 0
  protocol    = -1
  source_security_group_id = "${aws_security_group.builds_sg.id}"

  security_group_id = "${aws_security_group.builds_sg.id}"
}

resource "aws_security_group_rule" "ingress_22" {
  type        = "ingress"
  from_port   = 22
  to_port     = 22
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.builds_sg.id}"
}

resource "aws_security_group_rule" "ingress_80" {
  type        = "ingress"
  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.builds_sg.id}"
}

resource "aws_security_group_rule" "ingress_443" {
  type        = "ingress"
  from_port   = 443
  to_port     = 443
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.builds_sg.id}"
}

resource "aws_security_group_rule" "ingress_8080" {
  type        = "ingress"
  from_port   = 8080
  to_port     = 8080
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.builds_sg.id}"
}

resource "aws_security_group_rule" "ingress_8081" {
  type        = "ingress"
  from_port   = 8081
  to_port     = 8081
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.builds_sg.id}"
}

resource "aws_security_group_rule" "ingress_5432" {
  type        = "ingress"
  from_port   = 5432
  to_port     = 5432
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.builds_sg.id}"
}

resource "aws_security_group_rule" "ingress_5000" {
  type        = "ingress"
  from_port   = 5000
  to_port     = 5000
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.builds_sg.id}"
}

resource "aws_security_group_rule" "ingress_5001" {
  type        = "ingress"
  from_port   = 5001
  to_port     = 5001
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.builds_sg.id}"
}

resource "aws_security_group_rule" "ingress_2376" {
  type        = "ingress"
  from_port   = 2376
  to_port     = 2376
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.builds_sg.id}"
}

resource "aws_security_group_rule" "egress_all" {
  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = -1
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.builds_sg.id}"
}
