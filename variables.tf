# Specify the builds_key-pair as var
variable "builds_key_pair" {
    default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCqvzDcxgDIMkj/4HGGe7rrvmmWoQuihGdcmKurXUYN1PUwiPCI7cCvvJcjsF8QKmNeNbq2ncSUNjfUgE619oSPEY/37oQ/6hA50MWGBKAVR4NBiZdiZMbCMdqON8W1IaiGM0wtoPEIUcbkt/ThdFk/d4pXvLsRm1dbk3gmq917+oG7tJ9wb92jvhOBhJOJZ8OPPAqfkh2dFlgXWjaXNcUCpOLYo/579qCOw5P+t6rBbsO9Wvm1HK786m5K9hKPRHTZyFG0gX7WfuZ1aDLAts0xWGfYrRBEmizrbXFVjj+P08cSNP0hlNPCzHE4kYgRTtcRm6m0Tv+8ByKCoBZzRi8x jwilcox"
}

# Specify the private_key for provisioning development_setup
variable "ssh_key_private" {
    default = "/home/jwilcox/.ssh/builds.pem"
} 
# Specify the shared_credentials file as var
variable "creds" {
    default = "/home/jwilcox/.aws/credentials"
}              
               
# Specify the t2.micro instance_type as var
variable "t2_micro" {
    default = "t2.micro"
}              
               
# Specify the t2.large instance_type as var
variable "t2_large" {
    default = "t2.large"
}              
               
# Specify the "ubuntu-xenial-16.04-amd64-server-20180627 (ami-ba602bc2)" image as var
variable "ubuntu-xenial" {
    default = "ami-ba602bc2"
}              
               
# Specify the builds VPC address range as var
variable "builds_vpc_cidr" {
    default = "172.30.0.0/16"
}

# Specify the builds_subnet-2a address range as var
variable "sub_us-west-2a_cidr" {
    default = "172.30.0.0/24"
}

# Specify the builds_subnet-2b address range as var
variable "sub_us-west-2b_cidr" {
    default = "172.30.1.0/24"
}

# Specify the db VPC address range as var
variable "db_vpc_cidr" {
    default = "10.0.0.0/20"
}

# Specify the db private subnet-2c address range as var
variable "sub_priv_us-west-2c_cidr" {
    default = "10.0.8.0/21"
}

# Specify the db public subnet-2c address range as var
variable "sub_pub_us-west-2c_cidr" {
    default = "10.0.0.0/22"
}

# Specify an output var for the Builds ec2-instance id
output aws_instance_ids {
    value = ["${aws_instance.Builds.id}", "${aws_instance.Prod.id}"]
}
