# Cloud provider
provider "aws" {
  	region     = "us-west-2"
	shared_credentials_file = "${var.creds}"
}

# The builds VPC
resource "aws_vpc" "builds_vpc" {

    cidr_block  = "${var.builds_vpc_cidr}"
    enable_dns_hostnames = true
	tags {
	  Name = "builds_vpc"
	}
}

# The builds Internet Gateway
resource "aws_internet_gateway" "gw" {
    vpc_id = "${aws_vpc.builds_vpc.id}"
}

# Builds Public Subnet Oregon 2a
resource "aws_subnet" "sub_us-west-2a" {

    availability_zone       = "us-west-2a" 
    vpc_id                  = "${aws_vpc.builds_vpc.id}"
    cidr_block              = "${var.sub_us-west-2a_cidr}"
    map_public_ip_on_launch = true
    depends_on              = ["aws_internet_gateway.gw"]
    tags {
      Name = "sub_us-west-2a"
    }

}

# Builds Public Subnet Oregon 2b
resource "aws_subnet" "sub_us-west-2b" {

    availability_zone       = "us-west-2b" 
    vpc_id                  = "${aws_vpc.builds_vpc.id}"
    cidr_block              = "${var.sub_us-west-2b_cidr}"
    map_public_ip_on_launch = true
    depends_on              = ["aws_internet_gateway.gw"]
    tags {
      Name = "sub_us-west-2b"
    }

}

# The DB VPC
resource "aws_vpc" "db_vpc" {

    cidr_block  = "${var.db_vpc_cidr}"
    enable_dns_hostnames = true
	tags {
	  Name = "db-vpc"
	}
}

# The DB Internet Gateway
resource "aws_internet_gateway" "db-gw" {
    vpc_id = "${aws_vpc.db_vpc.id}"
}

# DB Public Subnet Oregon 2c
resource "aws_subnet" "sub_pub_us-west-2c" {

    availability_zone       = "us-west-2c" 
    vpc_id                  = "${aws_vpc.db_vpc.id}"
    cidr_block              = "${var.sub_pub_us-west-2c_cidr}"
    map_public_ip_on_launch = false
    depends_on              = ["aws_internet_gateway.db-gw"]
    tags {
      Name = "sub_pub_us-west-2c"
    }

}

# DB Private Subnet Oregon 2c
resource "aws_subnet" "sub_priv_us-west-2c" {

    availability_zone       = "us-west-2c" 
    vpc_id                  = "${aws_vpc.db_vpc.id}"
    cidr_block              = "${var.sub_priv_us-west-2c_cidr}"
    map_public_ip_on_launch = false
    depends_on              = ["aws_internet_gateway.db-gw"]
    tags {
      Name = "sub_priv_us-west-2c"
    }

}
